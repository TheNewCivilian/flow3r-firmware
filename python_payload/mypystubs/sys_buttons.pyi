def get_left() -> int: ...
def get_right() -> int: ...

PRESSED_LEFT: int
PRESSED_RIGHT: int
PRESSED_DOWN: int
NOT_PRESSED: int
